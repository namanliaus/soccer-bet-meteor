import { Mongo } from "meteor/mongo";
import { Schemas, Collections } from '../declarations';

Schemas.DateSum = new SimpleSchema({
  playerId: {
    type: String
  },
  userId: {
    type: String
  },
  date: {
    type: Date
  },
  win_sum: {
    type: Number,
    decimal: true
  },
  bet_sum: {
    type: Number,
    decimal: true
  },
  win_rate: {
    type: Number,
    decimal: true
  },
  win_point: {
    type: Number,
    decimal: true
  },
});

let DateSums = Collections.DateSums = new Mongo.Collection("datesums");
DateSums.attachSchema(Schemas.DateSum);

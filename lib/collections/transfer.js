import { Schemas, Collections } from "../declarations";
import { Mongo } from "meteor/mongo";

Schemas.Transfer = new SimpleSchema({
  from: {
    type: String
  },
  to: {
    type: String
  },
  amount: {
    type: Number,
    decimal: true,
  },
  when: {
    type: Date
  },
});

let Transfers = Collections.Transfers = new Mongo.Collection("transfers");
Transfers.attachSchema(Schemas.Transfer);

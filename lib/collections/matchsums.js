import { Mongo } from "meteor/mongo";
import { Schemas, Collections } from '../declarations';

Schemas.MatchSum = new SimpleSchema({
  playerId: {
    type: String
  },
  userId: {
    type: String
  },
  matchId: {
    type: String
  },
  matchTime: {
    type: Date
  },
  win_sum: {
    type: Number,
    decimal: true
  },
  bet_sum: {
    type: Number,
    decimal: true
  },
  win_rate: {
    type: Number,
    decimal: true
  },
  win_point: {
    type: Number,
    decimal: true
  },
});

let MatchSums = Collections.MatchSums = new Mongo.Collection("matchsums");
MatchSums.attachSchema(Schemas.MatchSum);

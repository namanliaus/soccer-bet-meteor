import { Collections } from "/lib/declarations";
import { initTimeFilter } from "../common/helpers";

Template.history_player_games.onCreated(function() {
  let self = this;
  self.autorun(function() {
    if (!initTimeFilter(-1, 0)) return;
    self.subscribe("history_player_games", Router.current().params._id);
  });
});

Template.history_player_games.helpers({
  initFilter() {
    if (!initTimeFilter(-1, 0)) return;
    return {time: {$lte: new Date()}};
  },
});

Template.history_player_games.events({
  "click .game-row_game"(e, t) {
    Router.go("history.game.player", {_id: this._id, playerId: Router.current().params._id});
  },
});

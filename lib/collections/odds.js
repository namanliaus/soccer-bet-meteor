import { Mongo } from "meteor/mongo";
import { SimpleSchema } from "meteor/aldeed:simple-schema";
import { Schemas, Collections } from "../declarations";

Schemas.Odd = new SimpleSchema({
  _id: {
    type: String
  },
  name: {
    type: String
  },
  value: {
    type: Number,
    decimal: true
  },
  win: {
    type: Boolean,
    optional: true
  }
});

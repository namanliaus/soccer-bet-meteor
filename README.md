# Project Descriptions #
Soccer Bet - A small betting game written with Meteor.js. Markets and odds are imported from williamhill.com.au.

----------

*Menu: an admin or operator has separated sections for running the game.*

![Menu](https://bytebucket.org/namanliaus/soccer-bet-meteor/raw/b404b9e63c95d7ad9f7a10963425d35bae93a627/doc-imgs/Clipboard01.jpg)

*Players contribute to the fund by donating. The contributions are converted into points. A player can transfer one's points to another.*

![Sponsors](https://bytebucket.org/namanliaus/soccer-bet-meteor/raw/b404b9e63c95d7ad9f7a10963425d35bae93a627/doc-imgs/Clipboard02.jpg)

*Game management: An admin or op can add new/edit/delete a game.*

![Game management](https://bytebucket.org/namanliaus/soccer-bet-meteor/raw/b404b9e63c95d7ad9f7a10963425d35bae93a627/doc-imgs/Clipboard03.jpg)

*Ranking: By match, by day, or overall showing players winning or losing states.*

![Ranking](https://bytebucket.org/namanliaus/soccer-bet-meteor/raw/b404b9e63c95d7ad9f7a10963425d35bae93a627/doc-imgs/Clipboard04.jpg)

*Update players' betting results by selecting winning odds.*

![Result updating](https://bytebucket.org/namanliaus/soccer-bet-meteor/raw/b404b9e63c95d7ad9f7a10963425d35bae93a627/doc-imgs/Clipboard05.jpg)

*History: history of a match shows bets, winning odds, and who is winning or losing.*

![History](https://bytebucket.org/namanliaus/soccer-bet-meteor/raw/b404b9e63c95d7ad9f7a10963425d35bae93a627/doc-imgs/Clipboard06.jpg)



## Project details ##

 1. Users can register and login with username/email/Facebook account/Google account.
 2. One who registered needs to wait for admin to approve to be a player with roles:
    Administrator / Operator / Normal.
 3. Admin can manage users and players. One can also manage games, import markets and odds for each game, and update results.
 4. Operator is a player with rights to update results.
 5. A player can see upcoming games and bet. One can view history of each day,
    each game, and other players in a game. Rankings are aggregated by game,
    by date, or overall.

## Environments ##
NodeJS and Meteor.js framework is multi-platform so an environment with them installed can host the project.
[Install Meteor](https://www.meteor.com/install)

### Development ###
* **OS:** Windows 10 was used but any OS should be fine.
* **Meteor** v1.3.2.4

## System Dependencies & Configuration ##
The good of Meteor framework is that it is almost zero config.

## Application Installation Instructions ##
 - Download and install Meteor from Meteor website for your OS. Make sure that you can execute command
```
>meteor
```
from a console window.
 - Clone or download source code of this project.
 - npm install mupx.
 - Deploy the application using mupx.

## Testing Instructions ##
N/A

## Contributing ##
Hoai-Nam Tran [nam.anliaus@gmail.com](mailto://nam.anliaus@gmail.com)
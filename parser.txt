let $text = $(text);
let sections = [];
$text.find("section").each(function(idx, el) {
  // parse a section
  let self = this;
  let odds = [];
  $(self).find("div.table.teams").each(function(idx, el) {
    // parse body containing odds
    let odd = $(this).find("div.buttons a");
    if (odd.data("link-competitorname") && odd.data("link-pricetowin")) {
      let small = odd.find("small");
      odds.push({
        name: odd.data("link-competitorname") + small.text().trim(),
        value: odd.data("link-pricetowin")
      });
    }
  });
  // save section
  sections.push({
    name: $(self).find(".drop-header > h3 > a.left").text().trim(),
    odds: odds
  })
});
return sections;

import { Meteor } from 'meteor/meteor';
import { moment } from "meteor/momentjs:moment";
import { Collections } from "/lib/declarations";
import { initTimeFilter } from "../common/helpers";

Template.player_games.onCreated(function() {
  let self = this;

  self.autorun(function() {
    if (!initTimeFilter(0, 1)) return;
    self.subscribe("games_future");
  });
});

Template.player_games.helpers({
  initFilter() {
    if (!initTimeFilter(0, 1)) return;
    return {time: {$gte: new Date()}};
  },
  now() {
    return moment();
  },
});

Template.player_games.events({
  "click .game-row_game"(e, t) {
    Router.go("player.games.game", {_id: this._id});
  },
});

import { Mongo } from "meteor/mongo";
import { Schemas, Collections } from '../declarations';

Schemas.Player = new SimpleSchema({
  userId: {
    type: String,
    unique: true
  },
  name: { // copy from user profile name
    type: String
  },
  init_point: {
    type: Number,
    defaultValue: 200
  },
  slips: {
    type: [String],
    optional: true
  },
  win_sum: {
    type: Number,
    decimal: true,
    optional: true
  },
  bet_sum: {
    type: Number,
    decimal: true,
    optional: true
  },
  win_rate: {
    type: Number,
    decimal: true,
    optional: true
  },
  win_point: {
    type: Number,
    decimal: true,
    optional: true
  },
});

let Players = Collections.Players = new Mongo.Collection("players");
Players.attachSchema(Schemas.Player);

// These schema and collection should be on client only. They keep only publish aggregated data
if (Meteor.isClient) {
  Collections.PlayerBalance = new Mongo.Collection("player_balance");
}

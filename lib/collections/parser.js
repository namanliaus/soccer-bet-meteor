import { Schemas, Collections } from "../declarations";
import { Mongo } from "meteor/mongo";

Schemas.Parser = new SimpleSchema({
  text: {
    type: String
  }
});

let Parser = Collections.Parser = new Mongo.Collection("parser");
Parser.attachSchema(Schemas.Parser);

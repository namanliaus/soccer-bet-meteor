import { Collections } from "/lib/declarations";
import { moment } from "meteor/momentjs:moment";
import { Router } from "meteor/iron:router";
import { initTimeFilter } from "../common/helpers";

Template.ranking_games.onCreated(function() {
  let self = this;

  self.autorun(function() {
    let timeFilter = initTimeFilter(-1, 0);
    if (!timeFilter) return;
    self.subscribe("games_past", {$gte: moment(timeFilter.startDate).toDate(), $lte: moment(timeFilter.endDate).endOf("day").toDate()});
  });
});

Template.ranking_games.helpers({
  initFilter() {
    if (!initTimeFilter(-1, 0)) return;
    return {time: {$lte: new Date()}};
  },
});

Template.ranking_games.events({
  "click .game-row_game"(e, t) {
    // go to the game ranking page
    Router.go("ranking.games.game", {_id: this._id});
  },
});

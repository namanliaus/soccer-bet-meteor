import { Collections } from "../lib/declarations";

/**
 * Publish a game and its slips for game history statistic
 */
Meteor.publishComposite("game_history_composite", function(gameId) {
  if (!this.userId || Roles.getRolesForUser(this.userId).length === 0) return [];
  return {
    find() {
      return Collections.Matches.find({_id: gameId, time: {$lte: new Date()}});
    },
    children: [{
      find(game) {
        return Collections.Slips.find({matchId: game._id});
      },
      children: [{
        find(slip) {
          return Collections.Players.find(slip.playerId);
        }
      }]
    }]
  }
});

/**
 * Publish a game and its slips for game history statistic
 */
Meteor.publishComposite("player_game_history", function(gameId, playerId) {
  if (!this.userId || Roles.getRolesForUser(this.userId).length === 0) return;
  return {
    find() {
      return Collections.Matches.find({_id: gameId, time: {$lte: new Date()}});
    },
    children: [{
      find(game) {
        return Collections.Slips.find({matchId: gameId, playerId: playerId});
      },
    }, {
      find(game) {
        return Collections.Players.find(playerId);
      }
    }]
  }
});

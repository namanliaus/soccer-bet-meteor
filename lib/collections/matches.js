import { Mongo } from "meteor/mongo";
import { SimpleSchema } from "meteor/aldeed:simple-schema";
import { Roles } from "meteor/alanning:roles";
import { Template } from "meteor/templating";
import { Schemas, Collections } from '../declarations';
// make sure Market schema is defined
import "./markets";

Schemas.Match = new SimpleSchema({
  name: {
    type: String
  },
  time: {
    type: Date
  },
  result: {
    type: String,
    optional: true
  },
  markets: {
    type: [Schemas.Market],
    optional: true
  }
});

let Matches = Collections.Matches = new Mongo.Collection("matches");
Matches.attachSchema(Schemas.Match);

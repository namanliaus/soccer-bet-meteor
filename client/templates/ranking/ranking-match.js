import { Collections } from "/lib/declarations";
import { Router } from "meteor/iron:router";

Template.ranking_game.onCreated(function() {
  let self = this;
  let gameId = Router.current().params._id;
  self.autorun(function() {
    self.subscribe("ranking_game", gameId);
    self.subscribe("players");
    self.subscribe("transfers");
    self.subscribe("one_game_in_past", gameId);
  });
});

Template.ranking_game.helpers({
  players() {
    let gameSums = Collections.MatchSums.find({matchId: Router.current().params._id}, {sort: {win_point: -1, win_rate: -1, bet_sum: -1}});
    return gameSums.map((gameSum, idx) => {
      let player = Collections.Players.findOne(gameSum.playerId);
      gameSum.position = idx + 1;
      gameSum.init_point = player.init_point;
      gameSum.name = player.name;
      return gameSum;
    });
  },
  game() {
    return Collections.Matches.findOne(Router.current().params._id);
  },
  gameDate() {
    return moment(this.time).format("dddd D MMM YYYY");
  },
});

Template.ranking_game.events({
  "click .rank-item"(e, t) {
    Router.go("history.game.player", {_id: Router.current().params._id, playerId: this.playerId});
  },
});

import { SimpleSchema } from "meteor/aldeed:simple-schema";
import { ReactiveVar } from "meteor/reactive-var";
import { moment } from "meteor/momentjs:moment";
import { Collections, Schemas } from "/lib/declarations";
import { initTimeFilter } from "../common/helpers";

Template.manage_games.onCreated(function() {
  let self = this;

  self.game = new ReactiveVar({
    method: "gameAdd"
  });

  self.autorun(function() {
    let timeFilter = initTimeFilter(0, 1);
    if (!timeFilter) return;
    self.subscribe("manage_games", {$gte: moment(timeFilter.startDate).toDate(), $lte: moment(timeFilter.endDate).endOf("day").toDate()});
  });
});

Template.manage_games.helpers({
  initFilter() {
    initTimeFilter(0, 1);
  },
  thisInstance() {
    return Template.instance();
  },
});

Template.manage_games.events({
  "click .game-row_game"(e, t) {
    $("#modalMenu").modal("show");
    // bind game data to menu
    $("#modalMenu").data("game", this);
  },
  "click .list-group-item"(e, t) {
    $("#modalMenu").modal("hide");
    let $target = $(e.currentTarget);
    let data = $("#modalMenu").data("game");
    if ($target.hasClass("js-menu-markets")) {
      Router.go("games.manage.markets", {_id: data._id});
      return;
    }

    if ($target.hasClass("js-menu-edit")) {
      editMatch(t, data);
      return;
    }

    if ($target.hasClass("js-menu-delete")) {
      deleteMatch(data);
      return;
    }
  },
  "click .js-game-markets"(e, t) {
    e.stopPropagation();
    Router.go("games.manage.markets", {_id: this._id});
    return;
  },
  "click .js-game-edit"(e, t) {
    e.stopPropagation();
    editMatch(t, this);
  },
  "click .js-game-delete"(e, t) {
    e.stopPropagation();
    deleteMatch(this)
  },
  "click .js-add-game"(e, t) {
    // toggle add game form
    t.$("#collapsibleForm").collapse("show");
    t.game.set({
      method: "gameAdd"
    });
  },
  "click .js-cancel"(e, t) {
    t.game.set({
      method: "gameAdd"
    });
    t.$("#collapsibleForm").collapse("hide");
  },
  "submit #formMatch"(e, t) {
    t.$("#collapsibleForm input").blur();
    t.$("#collapsibleForm").collapse("hide");
  },
});

Template.game_edit.helpers({
  schema: Schemas.Match,
  methodType() {
    let pp = Template.parentData(0).pp;
    return (pp && pp.game.get().method.substr("game".length) === "Add") ? "method" : "method-update";
  },
  method() {
    let pp = Template.parentData(0).pp;
    return pp && pp.game.get().method;
  },
  methodName() {
    let pp = Template.parentData(1).pp;
    return pp && pp.game.get().method.substr("game".length);
  },
  game() {
    return this.pp && this.pp.game.get();
  },
  dateTimePickerOptions: {
    format: "DD/MM/YYYY HH:mm",
    sideBySide: true
  }
});

let deleteMatch = function(data) {
  swal({
    title: "Are you sure?",
    text: "Deleting game.",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Confirm",
    closeOnConfirm: false,
  }, function() {
    Meteor.call("gameRemove", data._id, function(err, r) {
      if (err) {
        swal("Error", err.reason || err.message, "error");
      }
      swal.close();
    });
  });
};

let editMatch = function(t, data) {
  t.game.set({
    method: "gameUpdate",
    name: data.name,
    time: data.time,
    _id: data._id
  });
  t.$("#collapsibleForm").collapse("show");
  $("html, body").scrollTop(0);
};

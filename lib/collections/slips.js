import { Schemas, Collections } from "../declarations";
import { Mongo } from "meteor/mongo";

Schemas.Slip = new SimpleSchema({
  playerId: {
    type: String
  },
  userId: {
    type: String
  },
  matchId: {
    type: String
  },
  matchTime: {
    type: Date
  },
  marketIdx: {
    type: Number
  },
  oddIdx: {
    type: Number
  },
  oddValue: {
    type: Number,
    optional: true,
    decimal: true
  },
  bet: {
    type: Number,
    optional: true,
    decimal: true,
    min: 5
  },
  won: {
    type: Number,
    optional: true,
    decimal: true
  }
});

let Slips = Collections.Slips = new Mongo.Collection("slips");
Slips.attachSchema(Schemas.Slip);

import { Collections } from "/lib/declarations";

Template.player_of_game.onCreated(function() {
  let self = this;
  self.autorun(function() {
    self.subscribe("last_game");
    self.subscribe("players");
  });
});

Template.player_of_game.helpers({
  lastMatch() {
    let game = Collections.Matches.findOne({result: {$exists: 1}}, {sort: {time: -1}});
    if (!game) return;
    let t = Template.instance();
    t.subscribe("ranking_game", game._id);

    return game;
  },
  gameDate() {
    return moment(this.time).format("HH:mm dddd D MMM YYYY");
  },
  mostEffPlayer() {
    return {
      id: this._id,
      collection: Collections.MatchSums,
      filter: {matchId: this._id},
      options: {sort: {win_rate: -1}}
    }
  },
  mostWonPlayer() {
    return {
      id: this._id,
      collection: Collections.MatchSums,
      filter: {matchId: this._id},
      options: {sort: {win_point: -1}}
    }
  },
  mostBetPlayer() {
    return {
      id: this._id,
      collection: Collections.MatchSums,
      filter: {matchId: this._id},
      options: {sort: {bet_sum: -1}}
    }
  },
});

Template.player_of_game.events({
  "click .game-row_game"(e, t) {
    Router.go("history.game", {_id: this._id});
    $(window).scrollTop(0);
  },
  "click .home-player-item"(e, t) {
    Router.go("history.game.player", {_id: this.matchId, playerId: this.playerId});
    $(window).scrollTop(0);
  },
});

Template.home_player_item.helpers({
  player() {
    return this.selector.collection.findOne(this.selector.filter, this.selector.options);
  },
  playerName() {
    let player = Collections.Players.findOne(this.playerId);
    return player && player.name;
  },
  labelClass() {
    if (this.win_point > 0) return "success";
    else if (this.win_point === 0) return "primary";
    else return "danger";
  },
});

import { Meteor } from "meteor/meteor";
import { Router } from "meteor/iron:router";
import { Collections } from "/lib/declarations";
import { updateResult } from "./result-update";

Template.result_odd_pick.onCreated(function() {
  let self = this;
  self.autorun(function() {
    self.gameId = Iron.controller().getParams()._id;
    self.subscribe("one_game_in_past", self.gameId);
  });
  self.game = new ReactiveVar();
});

Template.result_odd_pick.helpers({
  game() {
    let template = Template.instance();
    let game = Collections.Matches.findOne(template.gameId);
    template.game.set(game);
    return game;
  },
  markets() {
    let template = Template.instance();
    let game = template.game.get();
    return game && game.markets;
  },
  gameDate() {
    return moment(this.time).format("dddd D MMM YYYY");
  },
});

Template.result_odd_pick.events({
  "click .js-odd"(e, t) {
    // toggle winning state
    let jqOdd = $(e.currentTarget);
    let currentState = jqOdd.hasClass("mark");
    let oddIdx = parseInt(jqOdd.data("index"));
    let marketIdx = parseInt(jqOdd.closest("div.panel-body").siblings().first().data("index"));
    // save odd
    Meteor.call("saveOdd", {matchId: t.gameId, marketIdx: marketIdx, oddIdx: oddIdx, win: !currentState});
  },
  "click .js-save"(e, t) {
    // pass the markets to servers for saving
    let game = t.game.get();
    Meteor.call("saveOdds", {matchId: t.gameId, markets: game.markets}, function(e, r) {
      if (e) {
        swal("Error", e.reason || e.message, "error");
        return;
      }
      swal("Odds saved.", "", "info");
    });
  },
  "click .game-row_game"(e, t) {
    updateResult.apply(this);
  },
});

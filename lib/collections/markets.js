import { Mongo } from "meteor/mongo";
import { SimpleSchema } from "meteor/aldeed:simple-schema";
import { Schemas, Collections } from "../declarations";
// make sure Odd schema has been defined
import "./odds"

Schemas.Market = new SimpleSchema({
  name: {
    type: String
  },
  odds: {
    type: [Schemas.Odd],
    optional: true
  }
});
